Attribute VB_Name = "Module_sub"
Option Explicit

' 変換後のaddr/data出力行
Const COL_ADDR As Long = 4
Const COL_DATA As Long = COL_ADDR + 1
' 変換前のaddr/data文字列長
Const LEN_ADDR As Long = 6
Const LEN_DATA As Long = 4

' シート名の最大長
Const MAX_SHEET_NAME_LEN As Long = 31

' UpdateTableの戻り値
Public Enum eRtnUpdateTbl
  UPDATE_OK
  UPDATE_NG
  UPDATE_SKIP
End Enum

' SearchTableIndexの戻り値
Public Enum eRtnSrchTblIdx
  SEARCH_OK
  NO_TABLE_NAME
  FORMAT_ERR
End Enum

' CheckTableDiffの戻り値
Public Enum eRtnChkTblDiff
  DIFF_EXIST
  DIFF_NONE
  DIFF_ERR
End Enum


' 現在Activeなブック/シート名を表示させるFunction
' [引数]
'   DispNum As Long：表示用通番
'
' [戻り値]
'   無し
Public Function DispActiveBS(ByRef DispNum As Long)
  Dim wb As Workbook
  Dim ws As Worksheet

  Set wb = ActiveWorkbook
  Set ws = ActiveSheet
  Debug.Print ("(" & DispNum & ")Active Workbook/Worksheet is " & _
               wb.Name & ":" & ws.Name)

  DispNum = DispNum + 1
End Function

' 処理結果＆差分表示クリア用Function
' [引数]
'   rowLast As Long：Table名が記載されている列の最終行
'
' [戻り値]
'   無し
Public Function ClearProcAndDiffResult(ByVal rowLast As Long)
  Dim i As Long

  For i = ROW_START To rowLast
    Cells(i, ColList.ProcResult) = ""
    Cells(i, ColList.DiffResult) = ""
  Next i

End Function

' ファイルオープン用Function
' [引数]
'   strFileName As String：ファイル名(Full Path)
'
' [戻り値]
'   Boolean：True  (オープン成功)
'            False (オープン失敗)
Public Function FileOpen(ByVal strBookName As String) As Boolean
  Dim buf As String
  Dim wb As Workbook

  Debug.Print "(FileOpen):strBookName = " & strBookName

  ' ファイルの存在チェック
  buf = Dir(strBookName)
  If buf = "" Then
    MsgBox strBookName & vbCrLf & "は存在しません", vbExclamation
    FileOpen = False
    Exit Function
  End If

  ' 同名ブックのチェック
  For Each wb In Workbooks
    If wb.Name = buf Then
      MsgBox buf & vbCrLf & "はすでに開いています", vbExclamation
      FileOpen = False
      Exit Function
    End If
  Next wb

  ' ここでブックを開く
  Workbooks.Open strBookName
  FileOpen = True

End Function

' ファイルクローズ用Function
' [引数]
'   strFileName As String：ファイル名
'
' [戻り値]
'   なし
Public Function FileClose(ByVal strBookName As String)

  Debug.Print "(FileClose):strBookName = " & strBookName

  ' 変更が保存されている場合はそのままClose
  If Workbooks(strBookName).Saved = True Then
    Workbooks(strBookName).Close
  ' 変更が保存されていない場合は上書き保存後にClose
  Else
    Workbooks(strBookName).Save
    Workbooks(strBookName).Close
  End If

End Function

' シートコピー用Function
' [引数]
'   strBookName As String：ブック名
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (コピー成功)
'          ：False (コピー失敗)
Public Function SheetCopy(ByVal strBookName As String, _
                          ByVal strSheetName As String) As Boolean
  Dim strCopySheetName As String
  Dim SheetNameLen As Long
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  ' 指定ブックに移動
  Workbooks(strBookName).Activate

  ' コピー先のシート名には、末尾に"_Old"を付ける
  strCopySheetName = strSheetName & "_Old"

  SheetNameLen = Len(strCopySheetName)
  ' コピー先シート名の文字数チェック("_Old"を含む最大31文字まで)
  If (SheetNameLen > MAX_SHEET_NAME_LEN) Then
    MsgBox "シート名は27文字以内にして下さい", vbExclamation
    SheetCopy = False
    Exit Function
  End If

  ' コピー元シートが無い場合、処理終了
  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    MsgBox strSheetName & "シートがありません", vbExclamation
    SheetCopy = False
    Exit Function
  End If

  ' コピー先シート名と同名シートがある場合、シートを削除する
  For Each ws In Worksheets
    If ws.Name = strCopySheetName Then
      Application.DisplayAlerts = False
      Worksheets(strCopySheetName).Delete
      Application.DisplayAlerts = True
      Exit For
    End If
  Next ws

  Application.DisplayAlerts = False
  Workbooks(strBookName).Worksheets(strSheetName).Copy After:= _
  Workbooks(strBookName).Worksheets(strSheetName)
  ActiveSheet.Name = strCopySheetName
  Application.DisplayAlerts = True

  SheetCopy = True

End Function

' 指定テーブルファイルに記載の作成日付を取得し、仕様書のヘッダ欄に反映する
' Function
' ※1.作成日付に変更が無い場合は、更新せずに終了する(return：False)
' ※2.仕様書側のテーブルヘッダに作成日付が無い場合、無条件で更新する(return：True)
' [引数]
'   strSpecBook As String：仕様書ブック名
'   strSpecSheet As String：仕様書シート名
'   strSpecTbl As String：テーブルファイル名
'
' [戻り値]
'   Boolean：True  (日付更新成功)
'            False (日付更新無し)
Public Function CheckCreateDate(ByVal strSpecBook As String, _
                                ByVal strSpecSheet As String, _
                                ByVal strSpecTbl As String) As Boolean
  Const ROW_COMMENT_DATE As Long = 3
  Const COL_COMMENT As Long = 2
  Dim TblNameIdx As Long
  Dim strCreated As String
  Dim CreatedPos As Long
  Dim strTblFile As String
  Dim buf As String, strComment As String
  Dim strCreateDate As String, strCommentDate As String

  Debug.Print "(CheckCreateDate):strSpecBook = " & strSpecBook & _
                               " strSpecSheet = " & strSpecSheet & _
                               " strSpecTbl = " & strSpecTbl

  strCreated = "Created On:"
  strCreateDate = ""

  Workbooks(strSpecBook).Worksheets(strSpecSheet).Activate

  ' 指定テーブルファイルより作成日付を読込む
  strTblFile = ThisWorkbook.Path & "\" & strSpecTbl & ".txt"

  Open strTblFile For Input As #1
    Do Until EOF(1)
      Line Input #1, buf
      ' 日付情報のみ読出す
      CreatedPos = InStr(buf, strCreated)
      If CreatedPos <> 0 Then
        strCreateDate = Mid(buf, CreatedPos)
        Debug.Print ("(CheckCreateDate):" & "strCreateDate = " & strCreateDate)
        Exit Do
      End If
    Loop
  Close #1

  ' 指定テーブルファイルに日付の記載が無かった場合、強制終了
  If strCreateDate = "" Then
    MsgBox strTblFile & vbCrLf & _
           "に日付の記載(Created On:)がありません"
    CheckCreateDate = False
    Exit Function
  End If

  ' 指定テーブルのコメント行(2行目)に日付があるか検索する
  strComment = Cells(TblIdx.rowTableStart + ROW_COMMENT_DATE, COL_COMMENT)
  CreatedPos = InStr(strComment, strCreated)
  ' 日付が無かった場合、仕様書のコメント行(2行目)に日付を追加して終了
  If CreatedPos = 0 Then
    Cells(TblIdx.rowTableStart + ROW_COMMENT_DATE, COL_COMMENT) = _
                                               strComment & " " & strCreateDate
    CheckCreateDate = True
    Exit Function
  ' 日付が有る場合、仕様書のコメント行(2行目)から日付を抜き出す
  Else
    strCommentDate = Mid(strComment, CreatedPos)
    Debug.Print ("(CheckCreateDate):" & "strCommentDate = " & strCommentDate)
  End If

  ' 指定テーブルファイルの作成日付と仕様書のコメント欄に記載の作成日付を比較
  ' 日付が同じ場合、そのまま終了
  If StrComp(strCreateDate, strCommentDate) = 0 Then
    CheckCreateDate = False
    Exit Function
  ' 日付が異なる場合、仕様書のコメント行(2行目)の日付を更新して終了
  Else
    strComment = Left(strComment, Len(strComment) - Len(strCommentDate))
    Debug.Print ("(CheckCreateDate):" & "strComment[pre] = " & strComment)
    strComment = strComment & strCreateDate
    Debug.Print ("(CheckCreateDate):" & "strComment = " & strComment)
    Cells(TblIdx.rowTableStart + ROW_COMMENT_DATE, COL_COMMENT) = strComment
    CheckCreateDate = True
    Exit Function
  End If

End Function

' 指定Tableに対応するファイルを読み込んで、シートにコピーするFunction
' [引数]
'   strTblName As String：テーブル名
'   strSheetName As String：作業用シート名
'
' [戻り値]
'   無し
Public Function ReadTable(ByVal strBookName As String, _
                          ByVal strTblName As String, _
                          ByVal strTempSheetName)
  Dim ws As Worksheet
  Dim wsNew As Worksheet
  Dim strTblFile As String
  Dim buf As String
  Dim rowIdx As Long
  Dim strPrePage As String

  Debug.Print "(ReadTable):strBookName = " & strBookName & _
                         " strTblName = " & strTblName & _
                         " strTempSheetName = " & strTempSheetName

  Workbooks(strBookName).Activate

  ' 指定シート名と同名シートがある場合、シートを削除する
  For Each ws In Worksheets
    If ws.Name = strTempSheetName Then
      Application.DisplayAlerts = False
      Worksheets(strTempSheetName).Delete
      Application.DisplayAlerts = True
      Exit For
    End If
  Next ws

  ' 新規シート作成
  Set wsNew = Worksheets.Add()
  wsNew.Name = strTempSheetName

  ' 作成したシートに指定Tableに対応するファイルを読込む
  Worksheets(strTempSheetName).Activate

  strTblFile = ThisWorkbook.Path & "\" & strTblName & ".txt"

  Open strTblFile For Input As #1
    rowIdx = 1
    strPrePage = "zz"

    Do Until EOF(1)
      Line Input #1, buf
      ' 直接アドレス→間接アドレスに変換
      Call TransTblAddrDA2IA(buf, rowIdx, strPrePage)
    Loop
  Close #1

  ' 最後に300ms Delayを追記
  Cells(rowIdx, COL_ADDR) = "0xFF"
  Cells(rowIdx, COL_DATA) = "0xC8"
  Cells(rowIdx + 1, COL_ADDR) = "0xFF"
  Cells(rowIdx + 1, COL_DATA) = "0x64"

End Function

' 直接アドレス→間接アドレスに変換するFunction
' [引数]
'   strInput As String：入力文字列(Tableファイル一行分)
'   rowOutIdx As Long：変換結果の出力用行カウンタ
'   strPre_P1 As String：ページ設定がある場合の前行保持アドレス
'
' [戻り値]
'   無し
Private Function TransTblAddrDA2IA(ByVal strInput As String, _
                                   ByRef rowOutIdx As Long, _
                                   ByRef strPre_P1 As String)
  Const PAGE_ADDR_LEN As Long = 1
  Const ADDR_DATA_LEN As Long = 2
  Static strP1 As String
  Dim strAddr As String, strData As String

  ' "0x"で始まる設定行の場合、以下を実行
  If (Left(strInput, Len("0x")) = "0x") Then
    ' 1行読み込み(Address, data)
    strAddr = Left(strInput, LEN_ADDR)
    strData = Right(strInput, LEN_DATA)

    ' ページ設定時の処理
    strP1 = Mid(strAddr, Len("0x00"), PAGE_ADDR_LEN)

    '最初、p1=1文字、pre_p1=2文字のため必ずこの条件に入る
    If strP1 <> strPre_P1 Then
      strPre_P1 = strP1
      'Address出力
      Cells(rowOutIdx, COL_ADDR) = "0x01"
      'data出力
      Cells(rowOutIdx, COL_DATA) = "0x0" & strP1
      rowOutIdx = rowOutIdx + 1
    End If

    '1行書き出し(ページ設定以外)
    'Address出力
    Cells(rowOutIdx, COL_ADDR) = Left(strAddr, Len("0x")) & _
                                 Right(strAddr, ADDR_DATA_LEN)
    'data出力
    Cells(rowOutIdx, COL_DATA) = strData
    rowOutIdx = rowOutIdx + 1
  ElseIf (strInput = "# Delay 300 msec") Then
    Cells(rowOutIdx, COL_ADDR) = "0xFF"  'Address出力
    Cells(rowOutIdx, COL_DATA) = "0xC8"  'data出力
    rowOutIdx = rowOutIdx + 1
    Cells(rowOutIdx, COL_ADDR) = "0xFF"  'Address出力
    Cells(rowOutIdx, COL_DATA) = "0x64"  'data出力
    rowOutIdx = rowOutIdx + 1
  End If

End Function

' 指定シートの指定範囲内に指定文字列がある場合、その行数を返すFunction
' [引数]
'   strAny As String ：任意の文字列
'   colTarget As Long：検索対象列
'   StartIdx As Long ：検索開始行
'   EndIdx As Long   ：検索終了行
'   FindIdx As Long  ：検索Hitした場合の行数
'
' [戻り値]
'   Boolean：True  (指定文字列の発見に成功)
'            False (指定文字列の発見に失敗)
Public Function SerchStrIndex(ByVal strAny As String, _
                              ByVal colTartget As Long, _
                              ByVal StartIdx As Long, _
                              ByVal EndIdx As Long, _
                              ByRef FindIdx As Long) As Boolean
  Dim i As Long
  Dim FindFlg As Boolean

  ' 指定文字列を検索
  FindFlg = False

  For i = StartIdx To EndIdx
    If Cells(i, colTartget) = strAny Then
      FindIdx = i
      FindFlg = True
      Exit For
    End If
  Next i

  If FindFlg = False Then
    SerchStrIndex = False
    Exit Function
  End If

  SerchStrIndex = True

End Function

' 指定ブック/シート/Tableを検索し、開始/終了行を返すFunction
' [引数]
'   strBookName As String：ブック名
'   strSheetName As String：シート名
'   strTableName As String：テーブル名
'   StartIndx
'
' [戻り値]
'   eRtnSrchTblIdx：SEARCH_OK       (正常終了)
'                   NO_TABLE_NAME   (TableNameの記載が無い)
'                   FORMAT_ERR      (FormatStart/End, TableStart/Endの記載が無い)
Public Function SearchTableIndex(ByVal strBookName As String, _
                                 ByVal strSheetName As String, _
                                 ByVal strTableName As String, _
                                 ByRef TableIndex As TableIdxGrp) As eRtnSrchTblIdx
  Const COL_SEARCH_TARGET As Long = 1
  Const ROW_SEARCH_START As Long = 1
  Dim rowLast As Long
  Dim rowFormatStart As Long
  Dim rowFormatEnd As Long
  Dim rowTableName As Long
  Dim strFormatStart As String, strFormatEnd As String
  Dim strTblStart As String, strTblEnd As String
  Dim strDataPos As String, strTemp1 As String, strTemp2 As String
  Dim StartPos As Long, EndPos As Long, DataPos As Long

  Debug.Print "(SearchTableIndex):strBookName = " & strBookName & _
                         " strSheetName = " & strSheetName & _
                         " strTableName = " & strTableName

  strFormatStart = "###FormatStart###"
  strFormatEnd = "###FormatEnd###"
  strTblStart = "###TableStart"
  strTblEnd = "###TableEnd###"
  strDataPos = "###TableStart|DataPos="

  ' シートに指定テーブルがあるか検索する
  Workbooks(strBookName).Worksheets(strSheetName).Activate

  ' Table名を取得
  TableIndex.TableName = strTableName

  rowLast = Cells(Rows.Count, 1).End(xlUp).row  ' 1列目の最終行を取得

  ' FormatStart行を取得
  If SerchStrIndex(strFormatStart, COL_SEARCH_TARGET, _
                   ROW_SEARCH_START, rowLast, rowFormatStart) = False Then
    MsgBox strBookName & "ブックの" & vbCrLf & _
           strSheetName & "シートに" & vbCrLf & _
           strFormatStart & "の記載がありません", vbExclamation
    SearchTableIndex = FORMAT_ERR
    Exit Function
  End If

  ' FormatEnd行を取得
  If SerchStrIndex(strFormatEnd, COL_SEARCH_TARGET, _
                   ROW_SEARCH_START, rowLast, rowFormatEnd) = False Then
    MsgBox strBookName & "ブックの" & vbCrLf & _
           strSheetName & "シートに" & vbCrLf & _
           strFormatEnd & "の記載がありません", vbExclamation
    SearchTableIndex = FORMAT_ERR
    Exit Function
  End If

  Debug.Print ("(SearchTableIndex):" & "rowFormatStart = " & rowFormatStart & _
                                      " rowFormatEnd = " & rowFormatEnd)

  ' 指定テーブル名がある行を取得
  If SerchStrIndex(strTableName, COL_SEARCH_TARGET, _
                   rowFormatStart, rowFormatEnd, rowTableName) = False Then
    MsgBox strBookName & "ブックの" & vbCrLf & _
           strSheetName & "シートに" & vbCrLf & _
           strTableName & "の記載がありません", vbExclamation
    SearchTableIndex = NO_TABLE_NAME
    Exit Function
  End If

  ' TableStart行を取得
  TableIndex.rowTableStart = rowTableName - 1

  If Left(Cells(TableIndex.rowTableStart, COL_SEARCH_TARGET), _
          Len(strTblStart)) <> strTblStart Then
    MsgBox strBookName & "ブックの" & vbCrLf & _
           strSheetName & "シートの" & vbCrLf & _
           strTableName & "に" & vbCrLf & _
           strTblStart & "の記載がありません", vbExclamation
    SearchTableIndex = FORMAT_ERR
    Exit Function
  End If

  ' TableEnd行を取得
  If SerchStrIndex(strTblEnd, COL_SEARCH_TARGET, _
                   rowTableName, rowFormatEnd, _
                   TableIndex.rowTableEnd) = False Then
    MsgBox strBookName & "ブックの" & vbCrLf & _
           strSheetName & "シートの" & vbCrLf & _
           strTableName & "に" & vbCrLf & _
           strTblEnd & "の記載がありません", vbExclamation
    SearchTableIndex = FORMAT_ERR
    Exit Function
  End If

  Debug.Print ("(SearchTableIndex):" & "rowTableName = " & rowTableName & _
                                      " rowTableStart = " & TableIndex.rowTableStart & _
                                      " rowTableEnd = " & TableIndex.rowTableEnd)

  ' 指定テーブルのaddr/data値が書かれている開始行を取得
  StartPos = Len(strDataPos)
  EndPos = InStr(Cells(TableIndex.rowTableStart, COL_SEARCH_TARGET), ",") - 1
  Debug.Print ("(SearchTableIndex):" & "StartPos = " & StartPos & _
                                      " EndPos = " & EndPos)

  strTemp1 = Cells(TableIndex.rowTableStart, COL_SEARCH_TARGET)
  strTemp2 = Mid(strTemp1, StartPos + 1, EndPos - StartPos)
  DataPos = Val(strTemp2)
  Debug.Print ("(SearchTableIndex):" & "strTemp1 = " & strTemp1 & _
                                      " strTemp2 = " & strTemp2 & _
                                      " DataPos = " & DataPos)

  TableIndex.rowAddrDataStart = TableIndex.rowTableStart + DataPos

  ' TableStart〜TableEndまでのTotal行
  TableIndex.rowTableTotal = TableIndex.rowTableEnd - TableIndex.rowTableStart

  SearchTableIndex = SEARCH_OK

End Function


' 指定シート/指定Tableに対応する直接→間接アドレス変換後データを
' コピーするFunction
' [引数]
'   strTblName As String：テーブル名
'   strSrcSheet As String：コピー元シート名
'   strDstSheet As String：コピー先シート名
'
' [戻り値]
'   無し
Public Function CopyTable(ByVal strBookName As String, _
                          ByVal strTblName As String, _
                          ByVal strSrcSheet As String, _
                          ByVal strDstSheet As String)
  Dim rowLastDst As Long, rowLastSrc As Long
  Dim i As Long
  Dim StartIdx As Long, LastIdx As Long
  Dim TblNameIdx As Long, TblStartIdx As Long, TblEndIdx As Long
  Dim strFormatStart As String, strFormatEnd As String
  Dim strTblStart As String, strTblEnd As String
  Dim strDataPos As String, strTemp1 As String, strTemp2 As String
  Dim StartPos As Long, EndPos As Long, DataPos As Long
  Dim DeleteRows As Range
  Dim strInsRng As String
  Dim ws As Worksheet

  Debug.Print "(CopyTable):strBookName = " & strBookName & _
                         " strTblName = " & strTblName & _
                         " strSrcSheet = " & strSrcSheet & _
                         " strDstSheet = " & strDstSheet

  Workbooks(strBookName).Worksheets(strDstSheet).Activate

  Set ws = Workbooks(strBookName).Worksheets(strDstSheet)
  rowLastDst = Cells(Rows.Count, 1).End(xlUp).row  ' 1列目の最終行を取得

  ' ヘッダを除いたaddr/data部分行を削除
  For i = TblIdx.rowAddrDataStart To TblIdx.rowTableEnd - 1
    If DeleteRows Is Nothing Then
      Set DeleteRows = ws.Rows(i).EntireRow
    Else
      Set DeleteRows = Union(DeleteRows, ws.Rows(i).EntireRow)
    End If
  Next i

  If Not DeleteRows Is Nothing Then DeleteRows.Delete

  Workbooks(strBookName).Worksheets(strSrcSheet).Activate

  ' コピー元シートのデータ行数分、コピー先に挿入する
  rowLastSrc = Cells(Rows.Count, COL_ADDR).End(xlUp).row  ' Addr記載列の最終行を取得

  Workbooks(strBookName).Worksheets(strDstSheet).Activate

  ' ###TableStart行 + DataPos(ヘッダ分) :
  ' 変換後データ最終行 + ###TableStart行 + DataPos(ヘッダ分) - 1
  strInsRng = TblIdx.rowAddrDataStart & ":" & _
              rowLastSrc + TblIdx.rowAddrDataStart - 1
  Debug.Print ("(CopyTable):strInsRng = " & strInsRng)
  Sheets(strDstSheet).Range(strInsRng).Insert

  ' コピー元シートのデータをコピー先シートの該当テーブル個所に挿入
  Sheets(strSrcSheet).Range("1:" & rowLastSrc).Copy Destination:= _
  Sheets(strDstSheet).Rows(TblIdx.rowAddrDataStart)

  ' コピー元シートを削除
  Application.DisplayAlerts = False
  Sheets(strSrcSheet).Delete
  Application.DisplayAlerts = True

End Function

' テーブル名に対応する設定ファイルを読み込み、直接→間接アドレスに変換後、
' 指定ブック/シート/テーブル名に対応する個所を更新するFunction
' [引数]
'   strSpecBook As String：仕様書ブック名
'   strSpecSheet As String：仕様書シート名
'   strSpecTbl As String：テーブルファイル名
'
' [戻り値]
'   eRtnUpdateTbl：UPDATE_OK (更新成功)
'                  UPDATE_NG (更新失敗)
'                  UPDATE_SKIP (更新SKIP)
Public Function UpdateTable(ByVal strSpecBook As String, _
                            ByVal strSpecSheet As String, _
                            ByVal strSpecTbl As String) As eRtnUpdateTbl
  Dim strTmpSheetName As String
  Dim SearchResult As eRtnSrchTblIdx
  Dim strTblFile As String

  strTmpSheetName = "temp"

  Debug.Print ("(UpdateTable):" & "strSpecBook = " & strSpecBook & _
                                 " strSpecSheet = " & strSpecSheet & _
                                 " strSpecTbl = " & strSpecTbl)

  SearchResult = SearchTableIndex(strSpecBook, strSpecSheet, strSpecTbl, TblIdx)

  Select Case SearchResult
    Case SEARCH_OK
      ' NOP (処理継続)
    Case NO_TABLE_NAME
      ' 新規テーブルのため、テンプレートの追加が必要な旨を通知
      MsgBox strSpecBook & "ブックの" & vbCrLf & _
             strSpecSheet & "シートに" & vbCrLf & _
             strSpecTbl & vbCrLf & _
             "のテンプレート(TableStart/End,Table名/ヘッダ)を追加して下さい", vbExclamation
      UpdateTable = UPDATE_NG
      Exit Function
    Case FORMAT_ERR
      UpdateTable = UPDATE_NG
      Exit Function
  End Select

  ' 指定テーブルに対応するファイルがあるかチェックする
  strTblFile = ThisWorkbook.Path & "\" & strSpecTbl & ".txt"

  If Dir(strTblFile) = "" Then
    MsgBox strTblFile & vbCrLf & "ファイルが見つかりません", vbExclamation
    UpdateTable = UPDATE_NG
    Exit Function
  End If

  ' 指定テーブルファイルに記載の作成日付を取得し、仕様書のヘッダ欄に反映する
  ' 作成日付に変更が無い場合、以降の処理は行わない
  If CheckCreateDate(strSpecBook, strSpecSheet, strSpecTbl) = False Then
    UpdateTable = UPDATE_SKIP
    Exit Function
  End If

  ' 指定テーブルを読込み直接アドレス→間接アドレスに変換後、指定シートにコピー
  Call ReadTable(strSpecBook, strSpecTbl, strTmpSheetName)

  ' 間接アドレスに変換後のaddr/dataが記載されたシートから、指定シートにコピーする
  Call CopyTable(strSpecBook, strSpecTbl, strTmpSheetName, strSpecSheet)

  UpdateTable = UPDATE_OK

End Function

' 指定ブック/シートと指定ブック/シート_Oldに記載の指定テーブルについて
' addr/valの簡易Diffを取り、結果を返すFunction
' [引数]
'   strSpecBook As String：仕様書ブック名
'   strSpecSheet As String：仕様書シート名
'   strSpecTbl As String：テーブルファイル名
'
' [戻り値]
'   eRtnChkTblDiff：DIFF_EXIST (変更有り)
'                   DIFF_NONE  (変更無し)
'                   DIFF_ERR   (異常終了)
Public Function CheckTableDiff(ByVal strBookName As String, _
                               ByVal strSheetName As String, _
                               ByVal strTableName As String) As eRtnChkTblDiff
  Dim strOldSheetName As String
  Dim SearchCurResult As eRtnSrchTblIdx
  Dim SearchOldResult As eRtnSrchTblIdx
  Dim rowTblTotal As Long, rowTblOldTotal As Long
  Dim i As Long
  Dim ws As Worksheet, ws_old As Worksheet

  strOldSheetName = strSheetName + "_Old"

  ' 「指定シート名」シートの指定テーブル開始行等を取得する
  SearchCurResult = SearchTableIndex(strBookName, strSheetName, _
                                     strTableName, TblIdx)
  Debug.Print ("(CheckTableDiff): SearchCurResult = " & SearchCurResult)

  Select Case SearchCurResult
    Case SEARCH_OK
      ' NOP (処理継続)
    Case NO_TABLE_NAME
      CheckTableDiff = DIFF_ERR
      Exit Function
    Case FORMAT_ERR
      CheckTableDiff = DIFF_ERR
      Exit Function
  End Select

  ' 「指定シート名_Old」シートの指定テーブル開始行等を取得する
  SearchOldResult = SearchTableIndex(strBookName, strOldSheetName, _
                                     strTableName, TblIdxOld)
  Debug.Print ("(CheckTableDiff): SearchOldResult = " & SearchOldResult)

  Select Case SearchOldResult
    Case SEARCH_OK
      ' NOP (処理継続)
    Case NO_TABLE_NAME
      CheckTableDiff = DIFF_ERR
      Exit Function
    Case FORMAT_ERR
      CheckTableDiff = DIFF_ERR
      Exit Function
  End Select

  ' 「指定シート」と「指定シート_Old」の指定テーブルTotal行が異なる場合、
  ' (変更有り)とみなす
  If TblIdx.rowTableTotal <> TblIdxOld.rowTableTotal Then
    Debug.Print ("(CheckTableDiff): TblIdx.rowTableTotal = " & TblIdx.rowTableTotal & _
                                  " TblIdxOld.rowTableTotal = " & TblIdxOld.rowTableTotal)
    CheckTableDiff = DIFF_EXIST
    Exit Function
  End If

  Set ws = Workbooks(strBookName).Worksheets(strSheetName)
  Set ws_old = Workbooks(strBookName).Worksheets(strOldSheetName)

  ' 「指定シート」と「指定シート_Old」の指定テーブルTotal行が同じ場合、
  ' 全セルを比較して差分が無ければ、(変更無し)とみなす
  For i = 0 To TblIdx.rowTableTotal - 1
    ' Addr値を比較
    If ws.Cells(TblIdx.rowTableStart + i, COL_ADDR) <> _
       ws_old.Cells(TblIdxOld.rowTableStart + i, COL_ADDR) Then
      Debug.Print ("(CheckTableDiff): DIFF_EXIST(Addr)" & _
                   " [" & (TblIdx.rowTableStart + i) & ":" & COL_ADDR & "] = " & _
                   ws.Cells(TblIdx.rowTableStart + i, COL_ADDR) & _
                   " [" & (TblIdxOld.rowTableStart + i) & ":" & COL_ADDR & "] = " & _
                   ws_old.Cells(TblIdxOld.rowTableStart + i, COL_ADDR))
      CheckTableDiff = DIFF_EXIST
      Exit Function
    End If

    ' Val値の比較
    If ws.Cells(TblIdx.rowTableStart + i, COL_DATA) <> _
       ws_old.Cells(TblIdxOld.rowTableStart + i, COL_DATA) Then
      Debug.Print ("(CheckTableDiff): DIFF_EXIST(val)" & _
                   " [" & (TblIdx.rowTableStart + i) & ":" & COL_DATA & "] = " & _
                   ws.Cells(TblIdx.rowTableStart + i, COL_DATA) & _
                   " [" & (TblIdxOld.rowTableStart + i) & ":" & COL_DATA & "] = " & _
                   ws_old.Cells(TblIdxOld.rowTableStart + i, COL_DATA))
      CheckTableDiff = DIFF_EXIST
      Exit Function
    End If
  Next i

  CheckTableDiff = DIFF_NONE

End Function

