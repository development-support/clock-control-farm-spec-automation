Attribute VB_Name = "Module_Main"
Option Explicit

' 各Tableの行数関連情報
Type TableIdxGrp
  TableName As String
  rowTableStart As Long
  rowTableEnd As Long
  rowAddrDataStart As Long
  rowTableTotal As Long
End Type

Public TblIdx As TableIdxGrp
Public TblIdxOld As TableIdxGrp

' Listシートの列番号情報
Type tColList
  BookName As Long
  SheetName As Long
  TableNo As Long
  TableName As Long
  SkipFlag As Long
  ProcResult As Long
  DiffResult As Long
End Type

Public ColList As tColList

' プログレスバー表示用Maxカウンタ
Public ProgressInfoMaxCounter As Long

' Active Workbook/Worksheet確認用番号
Public DispNo As Long

' Listシートのデータ検索開始行
Public Const ROW_START As Long = 3

' 処理結果欄の表示
Const STR_PROC_OK As String = "OK"
Const STR_PROC_NG As String = "NG"
Const STR_PROC_SKIP As String = "SKIP"

' 差分欄の表示
Const STR_DIFF_NONE As String = "変更無し"
Const STR_DIFF_EXIST As String = "変更有り"
Const STR_DIFF_ERR As String = "ERROR"
Const STR_DIFF_NA As String = "-"

' SKIPフラグの値
Const STR_NO_SKIP As String = "処理する"
Const STR_DO_SKIP As String = "処理しない"

'*******************************************************************************
' 仕様書作成ボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (1)指定仕様書名の指定シートをコピー(バックアップ)しておく
' (2)指定シート/テーブル名に対応する.txtファイル(直接アドレス設定データ)を読込む
' (3)(2)で読込んだ設定データを直接アドレス→間接アドレスに変換する
' (4)(3)で変換した設定データを、指定仕様書名の指定シートに記載していく
' (5)(1)〜(4)を全仕様書/シート/テーブルに対し、繰り返す
'*******************************************************************************
Private Sub btnMakeSpecification()
  Dim rowCur As Long, rowLast As Long
  Dim ReturnBook As String
  Dim ReturnSheet As String
  Dim strSpecBook As String
  Dim strSpecSheet As String
  Dim strTblName As String
  Dim strSkipFlag As String
  Dim strProcResult As String
  Dim strDiffResult As String
  Dim BookCell As Variant
  Dim SheetCell As Variant
  Dim TableCell As Variant
  Dim UpdateTblResult As eRtnUpdateTbl
  Dim CheckDiffResult As eRtnChkTblDiff

  DispNo = 0

  ' Call DispActiveBS(DispNo)

  ' セル名より、列番号を取得
  ColList.BookName = Range("BOOK_NAME").Column
  ColList.SheetName = Range("SHEET_NAME").Column
  ColList.TableNo = Range("TABLE_NO").Column
  ColList.TableName = Range("TABLE_NAME").Column
  ColList.SkipFlag = Range("SKIP_FLAG").Column
  ColList.ProcResult = Range("PROC_RESULT").Column
  ColList.DiffResult = Range("DIFF_RESULT").Column

  Debug.Print "(btnMakeSpecification):BooKName_Col = " & ColList.BookName & _
              " SheetName_Col = " & ColList.SheetName & _
              " TableNo_Col = " & ColList.TableNo & _
              " TableName_Col = " & ColList.TableName
  Debug.Print "                       SkipFlag_Col = " & ColList.SkipFlag & _
              " ProcResult_Col = " & ColList.ProcResult & _
              " DiffResult_Col = " & ColList.DiffResult

  strSpecBook = ""
  strSpecSheet = ""
  strTblName = ""

  ' 自身のBook名を取得
  ReturnBook = ActiveWorkbook.Name
  ReturnSheet = ActiveSheet.Name

  ' Table名が記載されている列の最終行を取得
  rowLast = Cells(Rows.Count, ColList.TableName).End(xlUp).row
  Debug.Print "(btnMakeSpecification):rowLast = " & rowLast

  ProgressInfoMaxCounter = rowLast
  ProgressInfo.Show vbModeless

  Application.ScreenUpdating = False

  ' 処理結果＆差分表示をクリア
  Call ClearProcAndDiffResult(rowLast)

  ' 最終行まで1行毎に処理
  For rowCur = ROW_START To rowLast

    With ProgressInfo
      .ProgressBar1.Value = rowCur
      .PercentOfProgress.Caption = Int(rowCur / rowLast * 100) & "%"
      .Repaint
    End With

    ' 元のブック/シートに戻る
    Workbooks(ReturnBook).Worksheets(ReturnSheet).Activate
    ' Call DispActiveBS(DispNo)

    ' Book名がある場合、取得
    If Cells(rowCur, ColList.BookName) <> "" Then
      ' 最初はBookをOpenするのみ
      If strSpecBook = "" Then
        ' NOP
      ' 既にOpenしたBookがある場合、Closeしてから再Openする
      Else
        FileClose (strSpecBook)
      End If

      strSpecBook = Cells(rowCur, ColList.BookName)

      If FileOpen(ThisWorkbook.Path & "\" & strSpecBook) = False Then
        ' プログレスバーの終了処理
        ProgressInfo.Hide
        Application.ScreenUpdating = True
        Unload ProgressInfo
        Exit Sub
      End If
    End If

    ' 元のブック/シートに戻る
    Workbooks(ReturnBook).Worksheets(ReturnSheet).Activate
    ' Call DispActiveBS(DispNo)

    ' Sheet名がある場合、取得
    If Cells(rowCur, ColList.SheetName) <> "" Then
      strSpecSheet = Cells(rowCur, ColList.SheetName)

      ' 指定されたブックのシートをコピー(バックアップ)する
      If SheetCopy(strSpecBook, strSpecSheet) = False Then
        ' プログレスバーの終了処理
        ProgressInfo.Hide
        Application.ScreenUpdating = True
        Unload ProgressInfo
        Exit Sub
      End If
    End If

    ' 元のブック/シートに戻る
    Workbooks(ReturnBook).Worksheets(ReturnSheet).Activate
    ' Call DispActiveBS(DispNo)

    ' Table名がある場合、取得
    If Cells(rowCur, ColList.TableName) <> "" Then
      strTblName = Cells(rowCur, ColList.TableName)
    ' Table名が無い場合、処理SKIP
    Else
      Debug.Print "(btnMakeSpecification):TableName is Empty. rowCur = " & rowCur
      ' 処理結果="SKIP", 差分="-"
      Cells(rowCur, ColList.ProcResult) = STR_PROC_SKIP
      Cells(rowCur, ColList.DiffResult) = STR_DIFF_NA
      GoTo SKIP_LABEL
    End If

    ' SKIPフラグを取得
    strSkipFlag = Cells(rowCur, ColList.SkipFlag)

    ' SKIPフラグ="処理しない"の場合のみ、処理SKIPする
    ' (SKIPフラグ="処理する"/空白の場合は処理する)
    If (strSkipFlag = STR_DO_SKIP) Then
      ' 処理結果="SKIP", 差分="-"
      Cells(rowCur, ColList.ProcResult) = STR_PROC_SKIP
      Cells(rowCur, ColList.DiffResult) = STR_DIFF_NA
      GoTo SKIP_LABEL
    End If

    ' 指定ブック/シート/テーブルに対応する個所を更新
    UpdateTblResult = UpdateTable(strSpecBook, strSpecSheet, strTblName)
    Debug.Print ("(btnMakeSpecification): UpdateTable = " & UpdateTblResult)

    ' 元のブック/シートに戻る
    Workbooks(ReturnBook).Worksheets(ReturnSheet).Activate
    ' Call DispActiveBS(DispNo)

    ' Update成功
    If UpdateTblResult = UPDATE_OK Then
      ' 処理結果="OK"
      Cells(rowCur, ColList.ProcResult) = STR_PROC_OK
      ' 差分チェック(簡易Diff)
      CheckDiffResult = CheckTableDiff(strSpecBook, strSpecSheet, strTblName)
      Debug.Print ("(btnMakeSpecification): CheckTableDiff = " & CheckDiffResult)

      ' 元のブック/シートに戻る
      Workbooks(ReturnBook).Worksheets(ReturnSheet).Activate
      ' Call DispActiveBS(DispNo)

      Select Case CheckDiffResult
        Case DIFF_EXIST
          ' 処理結果="OK", 差分="変更有り"
          Cells(rowCur, ColList.DiffResult) = STR_DIFF_EXIST
        Case DIFF_NONE
          ' 処理結果="OK", 差分="変更無し"
          Cells(rowCur, ColList.DiffResult) = STR_DIFF_NONE
        Case DIFF_ERR
          ' 処理結果="OK", 差分="ERROR"
          Cells(rowCur, ColList.DiffResult) = STR_DIFF_ERR
      End Select
    ' Update失敗
    ElseIf UpdateTblResult = UPDATE_NG Then
      ' 処理結果="NG", 差分="-"
      Cells(rowCur, ColList.ProcResult) = STR_PROC_NG
      Cells(rowCur, ColList.DiffResult) = STR_DIFF_NA
    ' Updateスキップ(テーブルファイル作成日付が同じ)
    Else
      ' 処理結果="SKIP", 差分="-"
      Cells(rowCur, ColList.ProcResult) = STR_PROC_SKIP
      Cells(rowCur, ColList.DiffResult) = STR_DIFF_NA
    End If

SKIP_LABEL:
  Next rowCur

  ' 最後にOpenしているBookをCloseする
  FileClose (strSpecBook)

  ProgressInfo.Hide
  Application.ScreenUpdating = True

  MsgBox "処理完了", vbInformation
  Unload ProgressInfo

End Sub

